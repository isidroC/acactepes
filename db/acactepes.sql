-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-05-2020 a las 02:51:15
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `acactepes`
--
CREATE DATABASE IF NOT EXISTS `acactepes` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `acactepes`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `analisis`
--

CREATE TABLE `analisis` (
  `id_analisis` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `opservacion` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha_creacion` date NOT NULL,
  `fecha_observacion` date DEFAULT NULL,
  `estado_analisis` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `analisis`
--

INSERT INTO `analisis` (`id_analisis`, `id_cliente`, `opservacion`, `fecha_creacion`, `fecha_observacion`, `estado_analisis`) VALUES
(2, 6, 'porque puedo y quiero ', '2020-05-03', '2020-05-07', 1),
(3, 7, 'erfhadfhsdfhsdfhsdhsdfhshf', '2020-05-05', '2020-05-08', 1),
(4, 8, 'strjfrjsrjjjsfrtjsrtjsfrj', '2020-05-05', '2020-05-08', 2),
(5, 9, 'porque tiene buen record ', '2020-05-08', '2020-05-08', 1),
(6, 10, 'porque tiene buen record', '2020-05-09', '2020-05-09', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nombres` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `dui` int(9) NOT NULL,
  `nit` int(14) NOT NULL,
  `id_sexo` int(11) NOT NULL,
  `direccion` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `id_municipio` int(11) NOT NULL,
  `profecion` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ingresos` float DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_spanish_ci DEFAULT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `eliminado` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nombres`, `apellidos`, `telefono`, `fecha_nacimiento`, `dui`, `nit`, `id_sexo`, `direccion`, `id_municipio`, `profecion`, `ingresos`, `email`, `id_usuario`, `fecha_creacion`, `eliminado`) VALUES
(6, 'Isidro Salvador', 'Arreaga', 78778410, '1990-01-01', 555555555, 2147483647, 1, 'Km 481/2 Carretera Troncal Del Norte Caserio El Coyolito Canton Quitasol Tejutla Chalatenango, Chalatenango', 154, 'sdfafeff', 3232.34, 'isidrosalvador1@gmail.com', 1, '2020-05-03', 1),
(7, 'Isidro Salvador', 'Arreaga', 78778410, '1990-01-01', 555552555, 2147483647, 1, 'Km 481/2 Carretera Troncal Del Norte Caserio El Coyolito Canton Quitasol Tejutla Chalatenango, Chalatenango', 351, 'nose ', 500, '', 1, '2020-05-05', 1),
(8, 'Leon', 'miguel', 49465251, '1990-01-01', 695515612, 2147483647, 1, 'Km 481/2 Carretera Troncal Del Norte Caserio El Coyolito Canton Quitasol Tejutla Chalatenango, Chalatenango', 1, 'sdfafeff', 545.45, '', 1, '2020-05-05', 1),
(9, 'Linda Fernanda', 'Melendez Henriquez', 75196523, '1996-08-26', 467858457, 2147483647, 2, 'casa 6 ,Colonia los arboles', 137, 'no tengo ', 300, '', 1, '2020-05-08', 1),
(10, 'Alex', 'Campos', 76823014, '1982-07-04', 564545646, 2147483647, 1, 'colonia no me acuerdo ', 137, 'docente', 5000, '', 3, '2020-05-09', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contratos`
--

CREATE TABLE `contratos` (
  `id_contrato` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `cuota` float NOT NULL,
  `monto` float NOT NULL,
  `monto_real` float NOT NULL,
  `plazo` float NOT NULL,
  `tasa` float NOT NULL,
  `mora` float NOT NULL,
  `total` float NOT NULL,
  `id_estado` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contratos`
--

INSERT INTO `contratos` (`id_contrato`, `id_cliente`, `fecha_inicio`, `fecha_fin`, `cuota`, `monto`, `monto_real`, `plazo`, `tasa`, `mora`, `total`, `id_estado`, `fecha_creacion`) VALUES
(2, 7, '2020-05-08', '2020-06-06', 1067.77, 5000, 10912.6, 60, 10.22, 7.8, 0, 1, '2020-05-08'),
(3, 7, '2020-05-08', '2020-05-30', 1067.77, 5000, 10912.6, 60, 10.22, 7.8, 0, 1, '2020-05-08'),
(4, 9, '2020-05-08', '2020-07-04', 49.91, 700, 510.08, 15, 10.22, 7.8, 0, 1, '2020-05-08'),
(5, 10, '2020-06-08', '2022-06-08', 23.12, 500, 236.286, 24, 10.22, 7.8, 0, 1, '2020-05-09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato_detalles`
--

CREATE TABLE `contrato_detalles` (
  `id_contrato_detalle` int(11) NOT NULL,
  `id_contrato` int(11) NOT NULL,
  `cuota_pagar` float NOT NULL,
  `fecha_pago` date NOT NULL,
  `fecha_pagada` date DEFAULT NULL,
  `mora_acumulada` float NOT NULL DEFAULT '0',
  `total_pagar` float NOT NULL,
  `total_detalles` float NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `contrato_detalles`
--

INSERT INTO `contrato_detalles` (`id_contrato_detalle`, `id_contrato`, `cuota_pagar`, `fecha_pago`, `fecha_pagada`, `mora_acumulada`, `total_pagar`, `total_detalles`) VALUES
(1, 2, 1067.77, '2020-05-08', NULL, 0, 1067.77, 0),
(2, 3, 1067.77, '2020-05-08', NULL, 0, 1067.77, 0),
(3, 4, 49.91, '2020-05-08', NULL, 0, 49.91, 0),
(4, 5, 23.12, '2020-06-08', NULL, 0, 23.12, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `creditos`
--

CREATE TABLE `creditos` (
  `id_credito` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `monto` float NOT NULL,
  `plazo` int(11) NOT NULL,
  `cuota` float NOT NULL,
  `tasa` float NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `eliminado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `creditos`
--

INSERT INTO `creditos` (`id_credito`, `nombre`, `monto`, `plazo`, `cuota`, `tasa`, `id_cliente`, `fecha_creacion`, `eliminado`) VALUES
(2, NULL, 500, 12, 46.65, 12.28, 6, '2020-05-03', 1),
(3, NULL, 5000, 60, 1067.77, 10.22, 7, '2020-05-05', 1),
(4, NULL, 5000, 60, 106.78, 10.22, 8, '2020-05-05', 1),
(5, NULL, 700, 15, 49.91, 10.22, 9, '2020-05-08', 1),
(6, NULL, 500, 24, 23.12, 10.22, 10, '2020-05-09', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departamento`
--

CREATE TABLE `departamento` (
  `id_departamento` int(11) NOT NULL,
  `departamento` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `departamento`
--

INSERT INTO `departamento` (`id_departamento`, `departamento`) VALUES
(1, 'Ahuachapán'),
(2, 'Cabañas'),
(3, 'Chalatenango'),
(4, 'Cuscatlán'),
(5, 'La Libertad'),
(6, 'La Paz'),
(7, 'La Unión'),
(8, 'Morazán'),
(9, 'San Miguel'),
(10, 'San Salvador'),
(11, 'San Vicente'),
(12, 'Santa Ana'),
(13, 'Sonsonate'),
(14, 'Usulután');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `id_estado` int(11) NOT NULL,
  `estado` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`id_estado`, `estado`) VALUES
(1, 'Activo'),
(2, 'Atrasado'),
(3, 'Inactivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_analisis`
--

CREATE TABLE `estado_analisis` (
  `id_estado` int(11) NOT NULL,
  `estado` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `estado_analisis`
--

INSERT INTO `estado_analisis` (`id_estado`, `estado`) VALUES
(1, 'aprobado'),
(2, 'denegado'),
(3, 'proceso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipio`
--

CREATE TABLE `municipio` (
  `id_municipio` int(11) NOT NULL,
  `id_departamento` int(11) NOT NULL,
  `municipio` varchar(40) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `municipio`
--

INSERT INTO `municipio` (`id_municipio`, `id_departamento`, `municipio`) VALUES
(1, 1, 'Ahuachapán'),
(2, 1, 'Apaneca'),
(3, 1, 'Atiquizaya'),
(4, 1, 'Concepción de Ataco'),
(5, 1, 'El Refugio'),
(6, 1, 'Guaymango'),
(7, 1, 'Jujutla'),
(8, 1, 'San Francisco Menéndez'),
(9, 1, 'San Lorenzo'),
(10, 1, 'San Pedro Puxtla'),
(11, 1, 'Tacuba'),
(12, 1, 'Turín'),
(13, 12, 'Candelaria de la Frontera'),
(14, 12, 'Chalchuapa'),
(15, 12, 'Coatepeque'),
(16, 12, 'El Congo'),
(17, 12, 'El Porvenir'),
(18, 12, 'Masahuat'),
(19, 12, 'Metapán'),
(20, 12, 'San Antonio Pajonal'),
(21, 12, 'San Sebastián Salitrillo'),
(22, 12, 'Santa Ana'),
(23, 12, 'Santa Rosa Guachipilín'),
(24, 12, 'Santiago de la Frontera'),
(25, 12, 'Texistepeque'),
(42, 13, 'Sonzacate'),
(43, 13, 'Acajutla'),
(44, 13, 'Armenia'),
(45, 13, 'Caluco'),
(46, 13, 'Cuisnahuat'),
(47, 13, 'Izalco'),
(48, 13, 'Juayúa'),
(49, 13, 'Nahuizalco'),
(50, 13, 'Nahulingo'),
(51, 13, 'Salcoatitán'),
(52, 13, 'San Antonio del Monte'),
(53, 13, 'San Julián'),
(54, 13, 'Santa Catarina Masahuat'),
(55, 13, 'Santa Isabel Ishuatán'),
(56, 13, 'Santo Domingo Guzmán'),
(57, 13, 'Sonsonate'),
(58, 13, 'Acajutla'),
(59, 13, 'Armenia'),
(60, 13, 'Caluco'),
(61, 13, 'Cuisnahuat'),
(62, 13, 'Izalco'),
(63, 13, 'Juayúa'),
(64, 13, 'Nahuizalco'),
(65, 13, 'Nahulingo'),
(66, 13, 'Salcoatitán'),
(67, 13, 'San Antonio del Monte'),
(68, 13, 'San Julián'),
(69, 13, 'Santa Catarina Masahuat'),
(70, 13, 'Santa Isabel Ishuatán'),
(71, 13, 'Santo Domingo Guzmán'),
(72, 13, 'Sonsonate'),
(73, 13, 'Sonzacate'),
(74, 3, 'Agua Caliente'),
(75, 3, 'Arcatao'),
(76, 3, 'Azacualpa'),
(77, 3, 'Chalatenango'),
(78, 3, 'Comalapa'),
(79, 3, 'Citalá'),
(80, 3, 'Concepción Quezaltepeque'),
(81, 3, 'Dulce Nombre de María'),
(82, 3, 'El Carrizal'),
(83, 3, 'El Paraíso'),
(84, 3, 'La Laguna'),
(85, 3, 'La Palma'),
(86, 3, 'La Reina'),
(87, 3, 'Las Vueltas'),
(88, 3, 'Nueva Concepción'),
(89, 3, 'Nueva Trinidad'),
(90, 3, 'Nombre de Jesús'),
(91, 3, 'Ojos de Agua'),
(92, 3, 'Potonico'),
(93, 3, 'San Antonio de la Cruz'),
(94, 3, 'San Antonio Los Ranchos'),
(95, 3, 'San Fernando'),
(96, 3, 'San Francisco Lempa'),
(97, 3, 'San Francisco Morazán'),
(98, 3, 'San Ignacio'),
(99, 3, 'San Isidro Labrador'),
(100, 3, 'San José Cancasque'),
(101, 3, 'San José Las Flores'),
(102, 3, 'San Luis del Carmen'),
(103, 3, 'San Miguel de Mercedes'),
(104, 3, 'San Rafael'),
(105, 3, 'Santa Rita'),
(106, 3, 'Tejutla'),
(107, 4, 'Candelaria'),
(108, 4, 'Cojutepeque'),
(109, 4, 'El Carmen'),
(110, 4, 'El Rosario'),
(111, 4, 'Monte San Juan'),
(112, 4, 'Oratorio de Concepción'),
(113, 4, 'San Bartolomé Perulapía'),
(114, 4, 'San Cristóbal'),
(115, 4, 'San José Guayabal'),
(116, 4, 'San Pedro Perulapán'),
(117, 4, 'San Rafael Cedros'),
(118, 4, 'San Ramón'),
(119, 4, 'Santa Cruz Analquito'),
(120, 4, 'Santa Cruz Michapa'),
(121, 4, 'Suchitoto'),
(122, 4, 'Tenancingo'),
(123, 10, 'Aguilares'),
(124, 10, 'Apopa'),
(125, 10, 'Ayutuxtepeque'),
(126, 10, 'Cuscatancingo'),
(127, 10, 'Ciudad Delgado'),
(128, 10, 'El Paisnal'),
(129, 10, 'Guazapa'),
(130, 10, 'Ilopango'),
(131, 10, 'Mejicanos'),
(132, 10, 'Nejapa'),
(133, 10, 'Panchimalco'),
(134, 10, 'Rosario de Mora'),
(135, 10, 'San Marcos'),
(136, 10, 'San Martín'),
(137, 10, 'San Salvador'),
(138, 10, 'Santiago Texacuangos'),
(139, 10, 'Santo Tomás'),
(140, 10, 'Soyapango'),
(141, 10, 'Tonacatepeque'),
(142, 5, 'Antiguo Cuscatlán'),
(143, 5, 'Chiltiupán'),
(144, 5, 'Ciudad Arce'),
(145, 5, 'Colón'),
(146, 5, 'Comasagua'),
(147, 5, 'Huizúcar'),
(148, 5, 'Jayaque'),
(149, 5, 'Jicalapa'),
(150, 5, 'La Libertad'),
(151, 5, 'Santa Tecla'),
(152, 5, 'Nuevo Cuscatlán'),
(153, 5, 'San Juan Opico'),
(154, 5, 'Quezaltepeque'),
(155, 5, 'Sacacoyo'),
(156, 5, 'San José Villanueva'),
(157, 5, 'San Matías'),
(158, 5, 'San Pablo Tacachico'),
(159, 5, 'Talnique'),
(160, 5, 'Tamanique'),
(161, 5, 'Teotepeque'),
(162, 5, 'Tepecoyo'),
(163, 5, 'Zaragoza'),
(164, 11, 'Apastepeque'),
(165, 11, 'Guadalupe'),
(166, 11, 'San Cayetano Istepeque'),
(167, 11, 'San Esteban Catarina'),
(168, 11, 'San Ildefonso'),
(169, 11, 'San Lorenzo'),
(170, 11, 'San Sebastián'),
(171, 11, 'San Vicente'),
(172, 11, 'Santa Clara'),
(173, 11, 'Santo Domingo'),
(174, 11, 'Tecoluca'),
(175, 11, 'Tepetitán'),
(176, 11, 'Verapaz'),
(177, 2, 'Cinquera'),
(178, 2, 'Dolores'),
(179, 2, 'Guacotecti'),
(180, 2, 'Ilobasco'),
(181, 2, 'Jutiapa'),
(182, 2, 'San Isidro'),
(183, 2, 'Sensuntepeque'),
(184, 2, 'Tejutepeque'),
(185, 2, 'Victoria'),
(186, 6, 'Cuyultitán'),
(187, 6, 'El Rosario'),
(188, 6, 'Jerusalén'),
(189, 6, 'Mercedes La Ceiba'),
(190, 6, 'Olocuilta'),
(191, 6, 'Paraíso de Osorio'),
(192, 6, 'San Antonio Masahuat'),
(193, 6, 'San Emigdio'),
(194, 6, 'San Francisco Chinameca'),
(195, 6, 'San Juan Nonualco'),
(196, 6, 'San Juan Talpa'),
(197, 6, 'San Juan Tepezontes'),
(198, 6, 'San Luis La Herradura'),
(199, 6, 'San Miguel Tepezontes'),
(200, 6, 'San Pedro Masahuat'),
(201, 6, 'San Pedro Nonualco'),
(202, 6, 'San Rafael Obrajuelo'),
(203, 6, 'Santa María Ostuma'),
(204, 6, 'Santiago Nonualco'),
(205, 6, 'Tapalhuaca'),
(206, 6, 'Zacatecoluca'),
(207, 14, 'Alegría'),
(208, 14, 'Berlín'),
(209, 14, 'California'),
(210, 14, 'Concepción Batres'),
(211, 14, 'El Triunfo'),
(212, 14, 'Ereguayquín'),
(213, 14, 'Estanzuelas'),
(214, 14, 'Jiquilisco'),
(215, 14, 'Jucuapa'),
(216, 14, 'Jucuarán'),
(217, 14, 'Mercedes Umaña'),
(218, 14, 'Nueva Granada'),
(219, 14, 'Ozatlán'),
(220, 14, 'Puerto El Triunfo'),
(221, 14, 'San Agustín'),
(222, 14, 'San Buenaventura'),
(223, 14, 'San Dionisio'),
(224, 14, 'San Francisco Javier'),
(225, 14, 'Santa Elena'),
(226, 14, 'Santa María'),
(227, 14, 'Santiago de María'),
(228, 14, 'Tecapán'),
(229, 14, 'Usulután'),
(290, 9, 'Carolina'),
(291, 9, 'Chapeltique'),
(292, 9, 'Chinameca'),
(293, 9, 'Chirilagua'),
(294, 9, 'Ciudad Barrios'),
(295, 9, 'Comacarán'),
(296, 9, 'El Tránsito'),
(297, 9, 'Lolotique'),
(298, 9, 'Moncagua'),
(299, 9, 'Nueva Guadalupe'),
(300, 9, 'Nuevo Edén de San Juan'),
(301, 9, 'Quelepa'),
(302, 9, 'San Antonio del Mosco'),
(303, 9, 'San Gerardo'),
(304, 9, 'San Jorge'),
(305, 9, 'San Luis de la Reina'),
(306, 9, 'San Miguel'),
(307, 9, 'San Rafael Oriente'),
(308, 9, 'Sesori'),
(309, 9, 'Uluazapa'),
(310, 8, 'Arambala'),
(311, 8, 'Cacaopera'),
(312, 8, 'Chilanga'),
(313, 8, 'Corinto'),
(314, 8, 'Delicias de Concepción'),
(315, 8, 'El Divisadero'),
(316, 8, 'El Rosario'),
(317, 8, 'Gualococti'),
(318, 8, 'Guatajiagua'),
(319, 8, 'Joateca'),
(320, 8, 'Jocoaitique'),
(321, 8, 'Jocoro'),
(322, 8, 'Lolotiquillo'),
(323, 8, 'Meanguera'),
(324, 8, 'Osicala'),
(325, 8, 'Perquín'),
(326, 8, 'San Carlos'),
(327, 8, 'San Fernando'),
(328, 8, 'San Francisco Gotera'),
(329, 8, 'San Isidro'),
(330, 8, 'San Simón'),
(331, 8, 'Sensembra'),
(332, 8, 'Sociedad'),
(333, 8, 'Torola'),
(334, 8, 'Yamabal'),
(335, 8, 'Yoloaiquín'),
(336, 7, 'Anamorós'),
(337, 7, 'Bolivar'),
(338, 7, 'Concepción de Oriente'),
(339, 7, 'Conchagua'),
(340, 7, 'El Carmen'),
(341, 7, 'El Sauce'),
(342, 7, 'Intipucá'),
(343, 7, 'La Unión'),
(344, 7, 'Lislique'),
(345, 7, 'Meanguera del Golfo'),
(346, 7, 'Nueva Esparta'),
(347, 7, 'Pasaquina'),
(348, 7, 'Polorós'),
(349, 7, 'San Alejo'),
(350, 7, 'San José'),
(351, 7, 'Santa Rosa de Lima'),
(352, 7, 'Yayantique'),
(353, 7, 'Yucuaiquín');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `referencias`
--

CREATE TABLE `referencias` (
  `id_referencia` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `telefono` int(11) NOT NULL,
  `fecha_creacion` date NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `eliminado` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `referencias`
--

INSERT INTO `referencias` (`id_referencia`, `nombre`, `telefono`, `fecha_creacion`, `id_cliente`, `eliminado`) VALUES
(3, 'rtvrter', 34232342, '2020-05-03', 6, 1),
(4, 'dgvfhjfgn', 12124124, '2020-05-03', 6, 1),
(5, 'mi tio ', 21541258, '2020-05-05', 7, 1),
(6, 'mi tia ', 86556855, '2020-05-05', 7, 1),
(7, 'Miguel leones ', 88855451, '2020-05-05', 8, 1),
(8, 'Lemira Leysi ', 85651512, '2020-05-05', 8, 1),
(9, 'Juana lopez', 45185455, '2020-05-08', 9, 1),
(10, 'jul lopez', 58464154, '2020-05-08', 9, 1),
(11, 'Miguel leones ', 54515154, '2020-05-09', 10, 1),
(12, 'mi tia ', 54646546, '2020-05-09', 10, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `rol` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `rol`) VALUES
(1, 'Admin'),
(2, 'Gerente'),
(3, 'Cobros'),
(4, 'Analista'),
(5, 'Asesor'),
(6, 'Cajera');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sexos`
--

CREATE TABLE `sexos` (
  `id_sexo` int(11) NOT NULL,
  `sexo` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sexos`
--

INSERT INTO `sexos` (`id_sexo`, `sexo`) VALUES
(1, 'Masculino'),
(2, 'Femenino');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `clave` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_registro` date NOT NULL,
  `id_rol` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `nombre`, `apellido`, `usuario`, `correo`, `clave`, `fecha_registro`, `id_rol`) VALUES
(1, 'Isidro', 'Colocho', 'isi', 'isidrosalvador1@gmail.com', '202cb962ac59075b964b07152d234b70', '2020-03-26', 1),
(2, 'leon', 'mendosa', 'leon', 'leon@gmail.com', '202cb962ac59075b964b07152d234b70', '2020-05-08', 4),
(3, 'mia', 'marines', 'mia', 'mia@gmail.com', '202cb962ac59075b964b07152d234b70', '2020-05-08', 5),
(4, 'nelson', 'Gonsales', 'nel', 'nelson@gmail.com', '202cb962ac59075b964b07152d234b70', '2020-05-08', 6),
(5, 'Miguel', 'Rnderos', 'jefe', 'miguel@gmail.com', '202cb962ac59075b964b07152d234b70', '2020-05-08', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `analisis`
--
ALTER TABLE `analisis`
  ADD PRIMARY KEY (`id_analisis`),
  ADD KEY `id_cliente` (`id_cliente`),
  ADD KEY `estado_analisis` (`estado_analisis`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`),
  ADD KEY `id_municipio` (`id_municipio`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_sexo` (`id_sexo`);

--
-- Indices de la tabla `contratos`
--
ALTER TABLE `contratos`
  ADD PRIMARY KEY (`id_contrato`);

--
-- Indices de la tabla `contrato_detalles`
--
ALTER TABLE `contrato_detalles`
  ADD PRIMARY KEY (`id_contrato_detalle`);

--
-- Indices de la tabla `creditos`
--
ALTER TABLE `creditos`
  ADD PRIMARY KEY (`id_credito`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indices de la tabla `departamento`
--
ALTER TABLE `departamento`
  ADD PRIMARY KEY (`id_departamento`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `estado_analisis`
--
ALTER TABLE `estado_analisis`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD PRIMARY KEY (`id_municipio`),
  ADD KEY `id_departamento` (`id_departamento`);

--
-- Indices de la tabla `referencias`
--
ALTER TABLE `referencias`
  ADD PRIMARY KEY (`id_referencia`),
  ADD KEY `id_cliente` (`id_cliente`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `sexos`
--
ALTER TABLE `sexos`
  ADD PRIMARY KEY (`id_sexo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD UNIQUE KEY `correo` (`correo`),
  ADD KEY `id_rol` (`id_rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `analisis`
--
ALTER TABLE `analisis`
  MODIFY `id_analisis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `contratos`
--
ALTER TABLE `contratos`
  MODIFY `id_contrato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `contrato_detalles`
--
ALTER TABLE `contrato_detalles`
  MODIFY `id_contrato_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `creditos`
--
ALTER TABLE `creditos`
  MODIFY `id_credito` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `estado_analisis`
--
ALTER TABLE `estado_analisis`
  MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `referencias`
--
ALTER TABLE `referencias`
  MODIFY `id_referencia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `sexos`
--
ALTER TABLE `sexos`
  MODIFY `id_sexo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `analisis`
--
ALTER TABLE `analisis`
  ADD CONSTRAINT `analisis_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`),
  ADD CONSTRAINT `analisis_ibfk_2` FOREIGN KEY (`estado_analisis`) REFERENCES `estado_analisis` (`id_estado`);

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`id_municipio`) REFERENCES `municipio` (`id_municipio`),
  ADD CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuarios` (`id_usuario`),
  ADD CONSTRAINT `clientes_ibfk_3` FOREIGN KEY (`id_sexo`) REFERENCES `sexos` (`id_sexo`);

--
-- Filtros para la tabla `creditos`
--
ALTER TABLE `creditos`
  ADD CONSTRAINT `creditos_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`);

--
-- Filtros para la tabla `municipio`
--
ALTER TABLE `municipio`
  ADD CONSTRAINT `municipio_ibfk_1` FOREIGN KEY (`id_departamento`) REFERENCES `departamento` (`id_departamento`);

--
-- Filtros para la tabla `referencias`
--
ALTER TABLE `referencias`
  ADD CONSTRAINT `referencias_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id_cliente`);

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
