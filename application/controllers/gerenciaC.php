<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class gerenciaC extends CI_Controller {

	public function __construct(){
		parent::__construct();
		/*$this->load->model('autentificar');*/
	}

	public function index()
	{
		if ($this->session->userdata('logueado') == TRUE) {
			$datos['title'] = 'Acactepes || Bienvenido';
			$this->load->view('templates/header',$datos);
			$this->load->view('gerencia/gerencia');
			$this->load->view('templates/footer');
		}else{
			redirect('login/index','refresh');
		}
	}

	
}