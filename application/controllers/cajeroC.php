<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CajeroC extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('CajeroM');
	}

	public function index()
	{
		if ($this->session->userdata('logueado') == TRUE) {
			$datos['title'] = 'Acactepes || cajero';
			$datos['contratos'] = $this->CajeroM->getContratos();
			$this->load->view('templates/header',$datos);
			$this->load->view('cajero/cajero');
			$this->load->view('templates/footer');
		}else{
			redirect('login/index','refresh');
		}
	}

	public function setCliente($id_contrato)
	{
		if ($this->session->userdata('logueado') == TRUE) {
			$datos['title'] = 'Acactepes || cajero';
			$datos['contratos'] = $this->CajeroM->getContrato($id_contrato);
			$datos['id_contrato'] = $id_contrato;
			$this->load->view('templates/header',$datos);
			$this->load->view('cajero/pagar');
			$this->load->view('templates/footer');
		}else{
			redirect('login/index','refresh');
		}
	}

	
}