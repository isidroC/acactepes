<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class asesorC extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('asesorM');

	}

	public function index()
	{
		if ($this->session->userdata('logueado') == TRUE) {
			
			$datos['title'] = 'Acactepes || Tipos de Creditos';
			$datos['sexo'] = $this->asesorM->getSexo();
			$datos['departamento'] = $this->asesorM->getDepartamento();
			$this->load->view('templates/header',$datos);
			$this->load->view('asesor/creditos');
			$this->load->view('templates/footer');

		}else{
			redirect('login/index','refresh');
		}
	}

	public function getMunicipio(){
		$id = $this->input->post('id');
		$data = $this->asesorM->getMunicipio($id);
		echo json_encode($data);
	}

	public function setCliente(){
		$data['monto'] = str_replace(',','',$this->input->post('monto'));
		$data['plazo'] = $this->input->post('plazo');
		$data['tasa'] = $this->input->post('tasa');
		$data['cuota'] = $this->input->post('cuota');
		$data['nombres'] = $this->input->post('nombres');
		$data['apellidos'] = $this->input->post('apellidos');
		$data['telefono'] = str_replace('-','',$this->input->post('telefono'));
		$data['fecha_n'] = $this->input->post('fecha_n');
		$data['dui'] = str_replace('-','',$this->input->post('dui'));
		$data['nit'] = str_replace('-','',$this->input->post('nit'));
		$data['sexo'] = $this->input->post('sexo');
		$data['municipio'] = $this->input->post('municipio');
		$data['direccion'] = $this->input->post('direccion');
		$data['ingresos'] = str_replace(',','',$this->input->post('ingresos'));
		$data['profeccion'] = $this->input->post('profeccion');
		$data['email'] = $this->input->post('email');
		$data['nombresRef1'] = $this->input->post('nombresRef1');
		$data['telefonoRef1'] = str_replace('-','',$this->input->post('telefonoRef1'));
		$data['nombresRef2'] = $this->input->post('nombresRef2');
		$data['telefonoRef2'] = str_replace('-','',$this->input->post('telefonoRef2'));
		$res = $this->asesorM->setCliente($data);
		echo json_encode($res);
	}

	public function Activos(){
		if ($this->session->userdata('logueado') == TRUE) {
			
			$datos['title'] = 'Acactepes || Tipos de Clientes';
			$datos['clientes'] = $this->asesorM->tipoClientes();
			$this->load->view('templates/header',$datos);
			$this->load->view('asesor/Activos');
			$this->load->view('templates/footer');

		}else{
			redirect('login/index','refresh');
		}
	}

	public function tipoClientes(){
		$data = $this->asesorM->tipoClientes();
		echo json_encode($data);
	}

	public function getCliente($id){
		if ($this->session->userdata('logueado') == TRUE) {
			
			$datos['title'] = 'Acactepes || Detalles de Cliente';
			$datos['cliente'] = $this->asesorM->getCliente($id);
			$datos['Credito'] = $this->asesorM->getCredito($id);
			$datos['Referido'] = $this->asesorM->getReferido($id);
			$datos['Analisis'] = $this->asesorM->getAnalisis($id);
			$this->load->view('templates/header',$datos);
			$this->load->view('asesor/Detalles');
			$this->load->view('templates/footer');

		}else{
			redirect('login/index','refresh');
		}
	}

	public function Contratacion($id_cliente,$id_analisis){
		if ($this->session->userdata('logueado') == TRUE) {
			
			$datos['title'] = 'Acactepes || Tipos de Clientes';
			$datos['clientes'] = $this->asesorM->tipoClientes();
			$datos['Credito'] = $this->asesorM->getCredito($id_cliente);
			$datos['id_cliente'] = $id_cliente;
			$datos['id_analisis'] = $id_analisis;
			$this->load->view('templates/header',$datos);
			$this->load->view('asesor/Contratacion');
			$this->load->view('templates/footer');

		}else{
			redirect('login/index','refresh');
		}
	}

	public function getCreditos(){
		$id = $this->input->post('id');
		$data = $this->asesorM->getCreditos($id);
		echo json_encode($data);
	}

	public function setContrato(){

		$data['id_cliente'] = $this->input->post('cliente');
		$data['id_analisis'] = $this->input->post('analisis');
		$url = $this->input->post('url');
		$data['monto'] = $this->input->post('monto');
		$data['plazo'] = $this->input->post('plazo');
		$data['tasa'] = $this->input->post('tasa');
		$data['cuota'] = $this->input->post('cuota');
		$data['fechaI'] = $this->input->post('fechaI');
		$fechaI = $this->input->post('fechaI');
		$data['fechaF'] = $this->input->post('fechaF');

		$imagen = $_FILES['imagen']['name'];

		$imagen = $this->asesorM->eliminar_acentos($imagen);

		$codigo = $this->asesorM->codigo();	

		$imagen = $fechaI."-".$codigo."-".$imagen;

		$data['imagen'] = $imagen;

		$imgpermitidos = array("image/jpg","image/jpeg","image/png");

		$limite_kb = 10000;



		/*if(in_array($_FILES['imagen']['type'],$imgpermitidos)){

			if($_FILES['imagen']['size'] <= $limite_kb * 5000){

                    //definiendo la ruta donde se subira la imagen

				$ruta = base_url()."pros/contratos".$imagen;

                    //comprobar so el archivo no existe en la ruta

                        //movemos el archivo al servidor

				$result = move($_FILES['imagen']['tmp_name'],$ruta);

				if($result){*/

					$id_contrato = $this->asesorM->setContrato($data);
					if ($id_contrato) {
						$data['id_contrato']= $id_contrato;
						$res = $this->asesorM->setContratoDetalle($data);
						if ($res) {
							$respuesta ="OK";
							echo json_encode($respuesta);
						}else{
							$respuesta ="noOk";
							echo json_encode("noOk");
						}
					}else{
						echo json_encode("noOk");
					}
				/*}else{
					echo json_encode("noServe");
				}
			}else{
				echo json_encode("grande");
			}
		}else{
			echo json_encode("grande");
		}*/
	}

}