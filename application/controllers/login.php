<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('autentificar');
	}

	public function index()
	{
		if ($this->session->userdata('logueado') == TRUE) {
			redirect('inicioC/index','refresh');
		}else{
			$this->load->view('login');
		}
	}

	public function validar(){
		$datos['usuario'] = $this->input->post('usuario');
		$datos['clave'] = $this->input->post('password');
		$data = $this->autentificar->validar($datos);
		if($data){
			//variables de sesion
			$datos_usuario = array('id' => $data->id_usuario, 'nombre'=> $data->nombre.' '.$data->apellido, 'rol'=> $data->id_rol,'logueado' => TRUE);
			$this->session->set_userdata($datos_usuario);
			echo json_encode(true);
			redirect('inicioC/index','refresh');
		}else{
			echo json_encode($data);
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login','refresh');
	}
}