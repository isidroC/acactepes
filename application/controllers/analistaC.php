<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnalistaC extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('AnalistaM');
		$this->load->model('asesorM');
	}

	public function index(){

		if ($this->session->userdata('logueado') == TRUE) {
			$datos['title'] = 'Acactepes || Bienvenido';
			$datos['clientes'] = $this->AnalistaM->tipoClientes();
			$this->load->view('templates/header',$datos);
			$this->load->view('analista/analista');
			$this->load->view('templates/footer');
		}else{
			redirect('login/index','refresh');
		}
	}

	public function getCliente($id){
		if ($this->session->userdata('logueado') == TRUE) {
			
			$datos['title'] = 'Acactepes || Detalles de Cliente';
			$datos['cliente'] = $this->asesorM->getCliente($id);
			$datos['Credito'] = $this->asesorM->getCredito($id);
			$datos['Referido'] = $this->asesorM->getReferido($id);
			$datos['Analisis'] = $this->asesorM->getAnalisis($id);
			$datos['datos'] = $this->AnalistaM->getAnalisisId($id);
			$datos['id']= $id;
			$this->load->view('templates/header',$datos);
			$this->load->view('analista/Aprobados');
			$this->load->view('templates/footer');

		}else{
			redirect('login/index','refresh');
		}
	}


	public function Aprobar(){
		$datos['id'] = $this->input->post('id');
		$datos['id_analisis'] = $this->input->post('id_analisis');
		$datos['observa'] = $this->input->post('observa');
		$res = $this->AnalistaM->Aprobar($datos);
		echo json_encode($res);
	}

	public function Negar(){
		$datos['id'] = $this->input->post('id');
		$datos['id_analisis'] = $this->input->post('id_analisis');
		$datos['observa'] = $this->input->post('observa');
		$res = $this->AnalistaM->Negar($datos);
		echo json_encode($res);
	}

	
}