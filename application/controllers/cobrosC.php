<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class cobrosC extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('CajeroC');
	}

	public function index()
	{
		if ($this->session->userdata('logueado') == TRUE) {
			$datos['title'] = 'Acactepes || Cobros';
			$datos['contratos'] = $this->CajerosC->getContratos();
			$this->load->view('templates/header',$datos);
			$this->load->view('cobros/cobros');
			$this->load->view('templates/footer');
		}else{
			redirect('login/index','refresh');
		}
	}

	
}