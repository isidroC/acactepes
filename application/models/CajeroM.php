<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CajeroM extends CI_Model {

	public function getContratos(){
		$this->db->select('c.id_contrato,cl.id_cliente,cl.dui,cl.nombres,cl.apellidos,cl.nit');
		$this->db->from('contratos c');
		$this->db->join('clientes cl','cl.id_cliente=c.id_cliente');
		$exe = $this->db->get();
		return $exe->result();
	}

	public function getContrato($id){
		$this->db->where('id_contrato',$id);
		$this->db->select('c.id_contrato,cl.id_cliente,cl.dui,cl.nombres,cl.apellidos,cl.nit');
		$this->db->from('contratos c');
		$this->db->join('clientes cl','cl.id_cliente=c.id_cliente');
		$exe = $this->db->get();
		return $exe->result();
	}



	/*public function validar($datos){
		//validamos que el usuario y la clave enviada pertenezcan al usuario
		$this->db->select('id_usuario,nombre,apellido,id_rol');
		$this->db->from('usuarios');
		$this->db->where('usuario',$datos['usuario']);
		$this->db->where('clave',md5($datos['clave']));
		$exe = $this->db->get();

		if ($this->db->affected_rows() > 0) {
			return $exe->row();
		}else{
			return false;
		}
	}*/
}
