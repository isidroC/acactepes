<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class asesorM extends CI_Model {

	public function getSexo(){
		$exe = $this->db->get('sexos');
		return $exe->result();
	}

	public function getDepartamento(){
		$exe = $this->db->get('departamento');
		return $exe->result();
	}

	public function getMunicipio($id){
		$this->db->where('id_departamento',$id);
		$exe = $this->db->get('municipio');
		return $exe->result();
	}

	public function setCliente($data){
		$id_usuario = $this->session->userdata('id');
		$cliente = $this->db->insert("clientes", 
			array(
				"nombres" => $data['nombres'],
				"apellidos" => $data['apellidos'],
				"telefono" => $data['telefono'], 
				"fecha_nacimiento" => $data['fecha_n'],
				"dui" => $data['dui'],
				"nit" => $data['nit'], 
				"id_sexo" => $data['sexo'],
				"direccion" => $data['direccion'],
				"id_municipio" => $data['municipio'],
				"profecion" => $data['profeccion'],
				"ingresos" => $data['ingresos'],
				"email" => $data['email'],
				"id_usuario" => $id_usuario,
				"fecha_creacion" => date('Y-m-d'),
				"eliminado" => 1
			)
		);
		
		if ($cliente) {
			
			$id_cliente = $this->db->insert_id();

			$creditos = $this->db->insert("creditos", 
				array(
					"monto" => $data['monto'],
					"plazo" => $data['plazo'], 
					"cuota" => $data['cuota'],
					"tasa" => $data['tasa'],
					"id_cliente" => $id_cliente,
					"fecha_creacion" => date('Y-m-d'),
					"eliminado" => 1
				)
			);
			if ($creditos) {
				$referencia1 = $this->db->insert("referencias",
					array(
						"nombre" => $data['nombresRef1'],
						"telefono" => $data['telefonoRef1'], 
						"id_cliente" => $id_cliente,
						"fecha_creacion" => date('Y-m-d'),
						"eliminado" => 1
					)
				);
				if ($referencia1) {
					$referencia2 = $this->db->insert("referencias",
						array(
							"nombre" => $data['nombresRef2'],
							"telefono" => $data['telefonoRef2'], 
							"id_cliente" => $id_cliente,
							"fecha_creacion" => date('Y-m-d'),
							"eliminado" => 1
						)
					);
					if ($referencia2) {
						$analisis = $this->db->insert("analisis",
							array(
								"id_cliente" => $id_cliente,
								"fecha_creacion" => date('Y-m-d'),
								"estado_analisis" => 3
							)
						);
						if ($analisis) {
							return 1;
						}else{
							return 0;
						}
					}else{
						return 0;
					}
				}else{
					return 0;
				}
			}else{
				return 0;
			}
			
		}else{
			return 0;
		}
	}

	public function tipoClientes(){
		$this->db->select('c.id_cliente,c.nombres,c.apellidos,c.telefono,c.dui,c.nit,c.fecha_creacion,a.opservacion,e.estado,e.id_estado');
		$this->db->join('analisis a','a.id_cliente=c.id_cliente');
		$this->db->join('estado_analisis e ',' e.id_estado=a.estado_analisis');
		return $this->db->get('clientes c')->result();
	}
	
	public function getCliente($id){
		return $this->db->select('c.id_cliente, CONCAT(c.nombres," ", c.apellidos) AS cliente, c.telefono, c.fecha_nacimiento, c.dui, c.nit,s.sexo, c.direccion, mu.municipio,de.departamento, c.profecion, c.ingresos, c.email, CONCAT(u.nombre," ",u.apellido) AS empleado, c.fecha_creacion')->join('sexos s ',' s.id_sexo=c.id_sexo')->join('municipio mu ',' mu.id_municipio=c.id_municipio')->join('departamento de ',' de.id_departamento=mu.id_departamento')->join('usuarios u ',' u.id_usuario=c.id_usuario')->where('id_cliente',$id)->get('clientes c')->result();

	}

	public function getCredito($id){
		return $this->db->where('id_cliente',$id)->get('creditos')->result();
	}

	public function getReferido($id){
		return $this->db->where('id_cliente',$id)->get('referencias')->result();
	}

	public function getAnalisis($id){
		return $this->db->where('id_cliente',$id)->get('analisis')->result();
	}

	public function aprobado(){
		$this->db->where('id_estado','3');
		$this->db->select('c.id_cliente,c.nombres,c.apellidos,c.telefono,c.dui,c.nit,c.fecha_creacion,a.fecha_observacion,a.opservacion,e.estado,e.id_estado');
		$this->db->join('analisis a','a.id_cliente=c.id_cliente');
		$this->db->join('estado_analisis e ',' e.id_estado=a.estado_analisis');
		return $this->db->get('clientes c')->result();
	}

	public function getCreditos($id){
		$this->db->where('id_credito',$id);
		$exe = $this->db->get('creditos');
		return $exe->result();
	}

	public function eliminar_acentos($cadena){

        //Reemplazamos la A y a
		$cadena = str_replace(
			array('Á', 'À', 'Â', 'Ä', 'á', 'à', 'ä', 'â', 'ª'),
			array('A', 'A', 'A', 'A', 'a', 'a', 'a', 'a', 'a'),
			$cadena
		);

        //Reemplazamos la E y e
		$cadena = str_replace(
			array('É', 'È', 'Ê', 'Ë', 'é', 'è', 'ë', 'ê'),
			array('E', 'E', 'E', 'E', 'e', 'e', 'e', 'e'),
			$cadena );

        //Reemplazamos la I y i
		$cadena = str_replace(
			array('Í', 'Ì', 'Ï', 'Î', 'í', 'ì', 'ï', 'î'),
			array('I', 'I', 'I', 'I', 'i', 'i', 'i', 'i'),
			$cadena );

        //Reemplazamos la O y o
		$cadena = str_replace(
			array('Ó', 'Ò', 'Ö', 'Ô', 'ó', 'ò', 'ö', 'ô'),
			array('O', 'O', 'O', 'O', 'o', 'o', 'o', 'o'),
			$cadena );

        //Reemplazamos la U y u
		$cadena = str_replace(
			array('Ú', 'Ù', 'Û', 'Ü', 'ú', 'ù', 'ü', 'û'),
			array('U', 'U', 'U', 'U', 'u', 'u', 'u', 'u'),
			$cadena );

        //Reemplazamos la N, n, C y c
		$cadena = str_replace(
			array('Ñ', 'ñ', 'Ç', 'ç'),
			array('N', 'n', 'C', 'c'),
			$cadena
		);

		return $cadena;
	}


	public function codigo(){
		$sqlName = "SELECT LEFT(MD5(RAND()), 4) as clave";

		$resName = $this->db->query($sqlName);

		if ($resName) {
			$dataName = $resName->row();
			$codigo = $dataName->clave;
			return $codigo;
		}else{
			return false;
		}

	}

	public function setContrato($data){
		$creditos = $this->db->insert("contratos", 
			array(
				"id_cliente" => $data['id_cliente'],
				"fecha_inicio" => $data['fechaI'],
				"fecha_fin" => $data['fechaF'],
				"cuota" => $data['cuota'],
				"monto" => $data['monto'],
				"monto_real" => $data['cuota']*$data['tasa'],
				"plazo" => $data['plazo'], 
				"tasa" => $data['tasa'],
				"mora" => '7.8',
				"total" => '0',
				"id_estado" => '1',
				"fecha_creacion" => date('Y-m-d')
			)
		);
		return $this->db->insert_id();
	}

	public function setContratoDetalle($data){
		$detalles = $this->db->insert("contrato_detalles", 
			array(
				"id_contrato" => $data['id_contrato'],
				"cuota_pagar" => $data['cuota'],
				"fecha_pago" => $data['fechaI'],
				"total_pagar" => $data['cuota'],
			)
		);
		return $detalles;
	}

}
