<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AnalistaM extends CI_Model {

	public function tipoClientes(){
		$this->db->select('c.id_cliente,c.nombres,c.apellidos,c.telefono,c.dui,c.nit,c.fecha_creacion,a.fecha_observacion,a.opservacion,e.estado,e.id_estado');
		$this->db->join('analisis a','a.id_cliente=c.id_cliente');
		$this->db->join('estado_analisis e ',' e.id_estado=a.estado_analisis');
		return $this->db->get('clientes c')->result();
	}

	public function Aprobar($datos){
		$this->db->where('id_analisis',$datos['id_analisis']);
		$this->db->set('opservacion', $datos['observa']);
		$this->db->set('fecha_observacion',date('Y-m-d'));
		$this->db->set('estado_analisis', '1');
		$res = $this->db->update('analisis');
		if ($res) {
			return 1;
		}else{
			return 0;
		}
	}

	public function Negar($datos){
		$this->db->where('id_analisis',$datos['id_analisis']);
		$this->db->set('opservacion', $datos['observa']);
		$this->db->set('fecha_observacion',date('Y-m-d'));
		$this->db->set('estado_analisis', '2');
		$res = $this->db->update('analisis');
		if ($res) {
			return 1;
		}else{
			return 0;
		}
	}
	public function getAnalisisId($id){
		$this->db->where('id_cliente',$id);
		$this->db->where('estado_analisis',3);
		$exe = $this->db->get('analisis');
		return $exe->row();
	}
}