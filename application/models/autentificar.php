<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class autentificar extends CI_Model {

	public function validar($datos){
		//validamos que el usuario y la clave enviada pertenezcan al usuario
		$this->db->select('id_usuario,nombre,apellido,id_rol');
		$this->db->from('usuarios');
		$this->db->where('usuario',$datos['usuario']);
		$this->db->where('clave',md5($datos['clave']));
		$exe = $this->db->get();

		if ($this->db->affected_rows() > 0) {
			return $exe->row();
		}else{
			return false;
		}
	}
}
