<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h4 class="page-title">Contrato</h4>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <center>
            <div>
                <h1>Contratacion de Acactepes</h1>
                <div class="col-6">
                    <form method="post" id="frmContrado" enctype="multipart/form-data">
                        <input type="hidden" name="cliente" id="cliente" value="<?= $id_cliente ?>">
                        <input type="hidden" name="analisis" id="analisis" value="<?= $id_analisis ?>">
                        <input type="hidden" name="url" id="url" value="<?= base_url() ?>">
                        <input type="hidden" name="cuota" id="cuota">
                        <input type="hidden" name="plazo" id="plazo">
                        <input type="hidden" name="tasa" id="tasa">
                        <input type="hidden" name="monto" id="monto">
                        <div class="form-group">
                            <label>Fecha de primer pago</label>
                            <br>
                            <input type="DATE" class="form-control" name="fechaI" id="fechaI" min="<?= date('Y-m-d')?>">
                        </div>
                        <div class="form-group">
                            <label>Fecha de ultimo pago</label>
                            <br>
                            <input type="DATE" class="form-control" name="fechaF" id="fechaF" min="<?= date('Y-m-d')?>">
                        </div>
                        <div class="form-group">
                            <label>Credito</label>
                            <br>
                            <select name="credito" id="credito" class="form-control">
                                <option value="">--seleccione un credito--</option>
                                <?php foreach ($Credito as $c) { ?>
                                    <option value="<?= $c->id_credito?>"><?php echo " Monto : ".$c->monto." "." Plazo : ".$c->plazo." "." Cuota : ".$c->cuota." "." Tasa : ".$c->tasa ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <input type="hidden" name="type" id="type">
                        <div class="form-group">
                          <label id="agregarImg" for="fname" class="control-label"><a style="color: black;" href="#" >Añadir Imagen de Contrato</a></label>

                          <label for="fname" class="control-label" id="CancelarIMG" style="display: none;"><a style="color: black;" href="#">Cancelar </a></label>

                          <div id="mostrarImg" style="display: none;">
                            <input type="file" id="imagen" name="imagen" class="form-control" accept="image/*">
                        </div>

                    </div>

                    <a style="float: left; margin-right: 3px;" href="<?= base_url('asesorC/activos') ?>" type="button" class="btn btn-default">Regresar</a>
                    <button style="float: right;" type="button" id="btnEnviar" class="btn btn-success" >Enviar</button>
                </form>
            </div>
        </div>
    </center>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer text-center">
    All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
<script type="text/javascript">
    $("#btnDenegar").click(function () {
        if ($("#observa").val().length<=10) {
            alert('Ingrege una Observacion mayor a 10 caracteres');
        }else{
            $("#frmOP").attr('action','<?= base_url("AnalistaC/Negar")?>');
            $url = $("#frmOP").attr('action');
            $data = $("#frmOP").serialize();
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: $url,
                data: $data,
                dataType: 'json',
                success: function (res) {
                    if (res==1) {
                        alertify.alert('Alerta', 'Credito Negado Exitosamente',function () {
                            window.location.href = "http://localhost/acactepes/AnalistaC/";
                        });
                    }else{
                        alertify.alert('Alerta', 'Error en Proceso');
                        
                    }
                }
            });
        }
    });

    $("#credito").change(function () {
        var id = $("#credito").val();
        var url = "<?= base_url('asesorC/getCreditos') ?>"
        $.ajax({
            type: 'ajax',
            method: 'post',
            url: url,
            data: {id:id},
            dataType: 'json',
            success: function (data) {
                for (var i=0 ; i < data.length ; i++) {
                    var monto = data[i].monto;
                    var plazo = data[i].plazo;
                    var tasa = data[i].tasa;
                    var cuota = data[i].cuota;                    
                }
                $("#monto").val(monto);
                $("#cuota").val(cuota);
                $("#plazo").val(plazo);
                $("#tasa").val(tasa);
            }
        });
    });

    $("#agregarImg").click(function () {
        document.getElementById('mostrarImg').style.display = 'block';
        document.getElementById('CancelarIMG').style.display = 'block';
        document.getElementById('agregarImg').style.display = 'none';
    });


    $("#CancelarIMG").click(function () {
        document.getElementById('mostrarImg').style.display = 'none';
        document.getElementById('agregarImg').style.display = 'block'; 
        document.getElementById('CancelarIMG').style.display = 'none';
    });

    $('#btnEnviar').on("click",function(e){
      if ($("#fechaI").val().length == 0 || $("#fechaF").val().length == 0) {
        alertify.alert('Alerta', 'llene todos los campos y agrege la imagen del contrato');
    }else{

        var typeUpd = 'notImg';


        if($('#imagen').val().length == 0){
            type = 'notImg';
        }else{
            type = 'Img';
        }

        document.getElementById('type').value = type;

        e.preventDefault();

        var formData= new FormData($('#frmContrado')[0]);



        var ruta = "<?= base_url('asesorC/setContrato') ?>";

        $.ajax({

          url:ruta, 

          type:"POST", 

          data: formData, 

          contentType:false, 

          processData:false, 

          success: function(result){

             switch(result){

                case '"OK"':
                alertify.alert('Alerta', 'Se guardo Exitosamente', function(){
                    location.reload();
                });

                break;

                case '"noOk"':
                alertify.alert('Alerta', 'ocurio un error, Intentelo mas tarde.');
                break;
                case 'noServe':
                 alertify.alert('Alerta', 'Imagen Error, no se pudo subir.');
                break;

                case 'grande':
                 alertify.alert('Alerta', 'Demaciado grande o formato no compatible.');
    
                break;

                default:
                 alertify.alert('Alerta', 'Ocurrió un error, intente más tarde...');

                break;

            }

        }

    });
        
    }

});

</script>
