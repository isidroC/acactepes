
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
<!--link rel="stylesheet" type="text/css" href="./bootstrap/css/jquery.dataTables.css"-->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap.css">


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.css">

<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">Detalles de Clientes</h1>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="container">
            <br>
            <center>
                <div>
                    <h3>Tramites en proceso</h3>
                </div>
                <div>
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">N°</th>
                                <th class="text-center">Observacion</th>
                                <th class="text-center">Fecha de Registro</th>
                                <th class="text-center">Fecha de respuesta</th>
                                <th class="text-center">Estado de proceso</th>
                                <th class="text-center" style="color: red;">Realizar contrato</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyActivos">
                            <?php $n=1; ?>
                            <?php foreach ($Analisis as $c) { ?>
                                <tr>
                                    <td class="text-center"><?php echo $n;$n++; ?></td>
                                    <td class="text-center">
                                        <?php if ($c->estado_analisis==3) {
                                            echo "Sin Observacion";
                                        }else{
                                            echo $c->opservacion;
                                        } ?>

                                    </td>
                                    <td class="text-center"><?= $c->fecha_creacion; ?></td>
                                    
                                    <td class="text-center">
                                        <?php if ($c->estado_analisis==3) {
                                            echo "En proceso";
                                        }elseif ($c->estado_analisis==2) {
                                            echo $c->fecha_observacion;
                                        }else{
                                            echo $c->fecha_observacion;
                                        } ?>
                                        
                                    </td>
                                    <td class="text-center">
                                        <?php if ($c->estado_analisis==3) {
                                            echo "En proceso de inspeccion";
                                        }elseif ($c->estado_analisis==2) {
                                            echo "denegado el prestamo";
                                        }else{
                                            echo "Aprobado listo para contrato";
                                        } ?>
                                        
                                    </td>
                                    <td class="text-center">
                                        <?php if ($c->estado_analisis==3 || $c->estado_analisis==2) { ?>
                                            
                                        <?php }else{ ?>
                                         <a href="<?= base_url('AsesorC/Contratacion/'.$c->id_cliente.'/'.$c->id_analisis) ?>" onclick="return confirm('¿Seguro que desea empesar el contrato de este cliente?')"><i class="fas fa-eye"></i>  
                                        <?php } ?>
                                        
                                    </td>
                                </tr>
                            <?php  } ?>
                        </tbody>
                        </tbody>
                    </table>
                </div>
            </center> 
            <div class="table-wrap" id="dvTable" >
                <div class="table-responsive">

                    <table class="table table-striped table-bordered" >
                        <thead>
                            <tr><td colspan="7"><center><h2>Detalles de cliente</h2></center></td></tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Dui</td>
                                <td class="text-center">Nombre Completo</td>
                                <td class="text-center">Nit</td>
                                <td class="text-center">Telefono</td>
                                <td class="text-center">Fecha de Nacimiento</td>
                                <td class="text-center">Sexo</td>
                                <td class="text-center">Departamento</td>
                                <td class="text-center">Municipio</td>
                                <td class="text-center">Direccion</td>
                                <td class="text-center">Profecion</td>
                                <td class="text-center">Ingresos</td>
                                <td class="text-center">Email</td>
                            </tr>
                        </tbody>
                        <tbody id="tbodyActivos">
                            <?php foreach ($cliente as $c) { ?>
                                <tr>
                                    <td class="text-center"><?= $c->dui; ?></td>
                                    <td class="text-center"><?= $c->cliente; ?></td>
                                    <td class="text-center"><?= $c->nit; ?></td>
                                    <td class="text-center"><?= $c->telefono; ?></td>
                                    <td class="text-center"><?= $c->fecha_nacimiento ?></td>
                                    <td class="text-center"><?= $c->sexo ?></td>
                                    <td class="text-center"><?= $c->departamento ?></td>
                                    <td class="text-center"><?= $c->municipio ?></td>
                                    <td class="text-center"><?= $c->direccion ?></td>
                                    <td class="text-center"><?= $c->profecion ?></td>
                                    <td class="text-center"><?= $c->ingresos ?></td>
                                    <td class="text-center"><?= $c->email ?></td>
                                </tr>
                            <?php  } ?>

                        </tbody>
                        <thead>
                            <tr><td colspan="5"><center><h2>Detalles de Credito</h2></center></td></tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Monto</td>
                                <td class="text-center">Plazo</td>
                                <td class="text-center">Cuota</td>
                                <td class="text-center">Tasa</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <?php foreach ($Credito as $c) { ?>
                                <tr>
                                    <td class="text-center"><?= $c->monto; ?></td>
                                    <td class="text-center"><?= $c->plazo; ?></td>
                                    <td class="text-center"><?= $c->cuota; ?></td>
                                    <td class="text-center"><?= $c->tasa; ?></td>
                                </tr>
                            <?php  } ?>
                        </tbody>
                        <thead>
                            <tr><td colspan="5"><center><h2>Referidos</h2></center></td></tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Nombre Completo</td>
                                <td class="text-center">Telefono</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <?php foreach ($Referido as $c) { ?>
                                <tr>
                                    <td class="text-center"><?= $c->nombre; ?></td>
                                    <td class="text-center"><?= $c->telefono; ?></td>
                                </tr>
                            <?php  } ?>
                        </tbody>
                        
                    </table>
                    <br>

                </div>
            </div>
            <a style="float: left;" href="<?= base_url('AsesorC')?>" onclick="return confirm('¿Seguro que quieres Registrar un credito nuevo y cliente?')" type="button" class="btn btn-primary">Registrar Creditos</a>
            <a style="float: left;" href="<?= base_url('AsesorC/Activos')?>" onclick="return confirm('¿Seguro que quieres Registrar un credito nuevo y cliente?')" type="button" class="btn btn-default">Regresar</a>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
<!--script type="text/javascript" src="./bootstrap/js/jquery.dataTables.min.js"></script-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>