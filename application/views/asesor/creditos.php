<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">Tipos de Creditos</h1>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- <div class="row">
            <div class="col-12"> -->
                <!-- Page Features -->
                <!-- <div class="row text-center"> -->
                    <center>
                        <form method="POST" name="formCreditos" id="formCreditos">
                            <div style="position: static; ">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label >Monto ($500 - $5000)</label><br>
                                                <input type="text" class="form-control" name="monto" id="monto" min="500" placeholder="monto entre 500 y 5000">
                                                <script type="text/javascript">
                                                    jQuery(function($){
                                                        $("#monto").maskMoney({prefix:'$ ', allowNegative: true, decimal:'.', affixesStay: false});
                                                    });
                                                </script>
                                            </div>
                                        </div>                                  
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label >Plazo ( Meses )</label><br>
                                                <input type="text" class="form-control" name="plazo" id="plazo" placeholder="entre 12 a 60" value="12" min="12" max="60" maxlength="2" onkeypress="return numeros(event)" >
                                            </div>
                                            <input type="hidden" name="tasa" id="tasa">
                                            <input type="hidden" name="cuota" id="cuota">        
                                        </div>
                                        <div class="col-sm">
                                            <div class="big-bullet">
                                                <div class="contenido text-120">Cuota Mensual</div>
                                            </div>
                                            <p id="hipo-cuota-txt"></p>
                                        </div>
                                        <div class="col-sm">
                                            <div class="big-bullet">
                                                <div class="contenido text-120">Monto </div>
                                            </div>
                                            <p id="hipo-monto-txt"></p>
                                        </div>
                                        <div class="col-sm">
                                            <div class="big-bullet">
                                                <div class="contenido text-120">Plazo</div>
                                            </div>
                                            <p id="hipo-tiempo-txt"></p>
                                        </div>
                                        <div class="col-sm">
                                            <div class="big-bullet">
                                                <div class="contenido text-120">Tasa</div>
                                            </div>
                                            <p id="hipo-tasa-txt"></p>
                                        </div>
                                    </div>

                                </div>


                                <br>
                                <div class="row">
                                    <div class="col-sm">
                                        <button class="btn btn-primary" id="btnCreditos" type="button">Aceptar Credito</button>  
                                    </div>
                                </div>
                            </div>
                            <br><br>
                            <div class="collapse" id="collapseExample">
                                <div class="container">
                                    <br>
                                    <center><h1>Ingrese los datos de el Cliente</h1></center>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Nombres*</label><br>
                                                <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="nombres" id="nombres" placeholder="Escriba los nombres">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Apellidos*</label><br>
                                                <input type="text" class="form-control" name="apellidos" onkeypress="return soloLetras(event)" id="apellidos" placeholder="Escriba los apellidos">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Telefono*</label><br>
                                                <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Escriba el numero de telefono">
                                                <script type="text/javascript">
                                                    jQuery(function($){
                                                        $("#telefono").mask('9999-9999');
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>fecha de nacimiento*</label><br>
                                                <input type="date" class="form-control" name="fecha_n" id="fecha_n" placeholder="fecha de nacimiento"  min="1920-01-01" value="1990-01-01" max="<?= date('Y-m-d'); ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>DUI*</label><br>
                                                <input type="text" class="form-control" name="dui" id="dui" placeholder="Ingrese el Dui">
                                                <script type="text/javascript">
                                                    jQuery(function($){
                                                        $("#dui").mask('99999999-9');
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>NIT*</label><br>
                                                <input type="text" class="form-control" name="nit" id="nit" placeholder="Ingrese el NIT">
                                                <script type="text/javascript">
                                                    jQuery(function($){
                                                        $("#nit").mask('9999-999999-999-9');
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Sexo</label><br>
                                                <select name="sexo" id="sexo" class="form-group">
                                                    <option value="">Escoja una Opcion</option>
                                                    <?php foreach ($sexo as $s) { ?>
                                                        <option value="<?= $s->id_sexo ?>"><?= $s->sexo ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Departamento</label><br>
                                                <select name="departamento" id="departamento" class="form-group">
                                                    <option value="">Escoja una Opcion</option>
                                                    <?php foreach ($departamento as $s) { ?>
                                                        <option value="<?= $s->id_departamento ?>"><?= $s->departamento ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Municipio</label><br>
                                                <select name="municipio" id="municipio" class="form-group">

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Direccion*</label><br>
                                                <input type="text" class="form-control" name="direccion" id="direccion" placeholder="Escriba su direccion">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Profesion*</label><br>
                                                <input type="text" class="form-control" name="profeccion" id="profeccion" placeholder="Escriba su profecion">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Ingresos*</label><br>
                                                <input type="text" class="form-control" name="ingresos" id="ingresos" placeholder="Escriba su sus ingresos">
                                                <script type="text/javascript">
                                                    jQuery(function($){
                                                        $("#ingresos").maskMoney({prefix:'$ ', allowNegative: true, decimal:'.', affixesStay: false});
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Email </label><br>
                                                <input type="EMAIL" class="form-control" onchange="validarEmail()" name="email" id="email" placeholder="Escriba su Email">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <center><h3>Ingrese 2 personas de contacto</h3></center>
                                    <br><br>
                                    <div class="row">
                                        <div>
                                            <h4>Contacto 1</h4>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Nombres Completos*</label><br>
                                                <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="nombresRef1" id="nombresRef1" placeholder="Escriba los nombres">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Telefono*</label><br>
                                                <input type="text" class="form-control" name="telefonoRef1" id="telefonoRef1" placeholder="Escriba el nuemro de telefono">
                                                <script type="text/javascript">
                                                    jQuery(function($){
                                                        $("#telefonoRef1").mask('9999-9999');
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        <div>
                                            <h4>Contacto 2</h4>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Nombres Completos*</label><br>
                                                <input type="text" class="form-control" onkeypress="return soloLetras(event)" name="nombresRef2" id="nombresRef2" placeholder="Escriba los nombres">
                                            </div>
                                        </div>
                                        <div class="col-sm">
                                            <div class="form-group">
                                                <label>Telefono*</label><br>
                                                <input type="text" class="form-control" name="telefonoRef2" id="telefonoRef2" placeholder="Escriba el nuemro de telefono">
                                                <script type="text/javascript">
                                                    jQuery(function($){
                                                        $("#telefonoRef2").mask('9999-9999');
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                            </div>
                        </form>
                        <div class="container">
                            <button class="btn-primary" id="btnRegistrarC">Registrar Cliente</button>
                        </div>
                    </center>
               <!-- </div>
            </div>
        </div> -->
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer text-center">
    All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
<script type="text/javascript">

    $("#btnRegistrarC").hide();


    $(document).ready(function () {
        $("#monto").blur(function () {
            var monto = $("#monto").val();
            var plazo = $("#plazo").val();
            var tasa = $("#tasa").val();
            var cuota = "";
            monto = monto.replace(/,/g, "");
            if(monto<500){
                alertify.alert('Alerta', 'El Monto tiene que ser mayor a 500');
                $("#monto").val('500');
            }
            if(monto>5000){
                alertify.alert('Alerta', 'El Monto tiene que ser menor a 500');
                $("#monto").val('5000');
            }
            if (plazo<13) {
                tasa=12.28;
            }else{
                tasa = 10.22;
            }
            cuota = ((tasa/(100*12))*monto)/(1-(Math.pow((1+(tasa/(100*12))),-plazo)));
            cuota = cuota.toFixed(2);
            $("#hipo-cuota-txt").html('$'+cuota);
            $("#cuota").val(cuota);
            $("#hipo-monto-txt").html("$"+monto);
            $("#hipo-tiempo-txt").html(plazo+" Meses");
            $("#hipo-tasa-txt").html(tasa+'%');
            $("#tasa").val(tasa);
        });

        $("#plazo").blur(function () {
            var monto = $("#monto").val();
            var plazo = $("#plazo").val();
            var tasa = $("#tasa").val();
            var cuota = "";
            monto = monto.replace(/,/g, "");
            if(plazo<6){
                alertify.alert('Alerta', 'El plazo tiene que ser mayor a 6');
                $("#plazo").val('6');
            }
            if(plazo>60){
                alertify.alert('Alerta', 'El plazo tiene que ser menor a 60');
                $("#plazo").val('60');
            }
            if (plazo<13 && plazo >= 6) {
                tasa=12.28;
            }else{
                tasa = 10.22;
            }
            cuota = ((tasa/(100*12))*monto)/(1-(Math.pow((1+(tasa/(100*12))),-plazo)));
            cuota = cuota.toFixed(2);
            $("#hipo-cuota-txt").html('$'+cuota);
            $("#cuota").val(cuota);
            $("#hipo-monto-txt").html("$"+monto);
            $("#hipo-tiempo-txt").html(plazo+" Meses");
            $("#hipo-tasa-txt").html(tasa+'%');
            $("#tasa").val(tasa);
        });

        $("#departamento").change(function () {
            var id = $("#departamento").val();
            var url = '<?= base_url('asesorC/getMunicipio'); ?>';
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: url,
                data: {id:id},
                dataType: 'json',

                success: function (data) {
                    console.log(data);
                    var res = '';
                    res+='<option value="">Escoja una opcion</option>';
                    for (var i = 0; i < data.length ; i++) {
                        res+='<option value="'+data[i].id_municipio+'">'+data[i].municipio+'</option>';
                    }
                    $("#municipio").html(res);
                }
            });
        });

        $("#btnRegistrarC").click(function () {
            if ($("#nombres").val().length==0 || $("#apellidos").val().length==0 || $("#dui").val().length==0 || $("#nit").val().length==0 || $("#telefono").val().length==0 || $("#fecha_n").val().length==0 || $("#sexo").val()=="" || $("#municipio").val() =="" || $("#direccion").val().length==0 || $("#profeccion").val().length==0 || $("#ingresos").val().length==0 || $("#nombresRef2").val().length==0 || $("#nombresRef1").val().length==0 || $("#telefonoRef1").val().length==0 || $("#telefonoRef2").val().length==0) {

                alertify.alert('Alerta', 'Llene todos los campos con "*" del los clientes');
                return false;

            }else{
                var url = "<?= base_url('asesorC/setCliente') ?>";
                var data = $("#formCreditos").serialize();
                $.ajax({
                    type: 'ajax',
                    method: 'post',
                    url: url,
                    data: data,
                    dataType: 'json',
                    success: function (res) {
                        if (res == '1') {
                            alertify.alert('Alerta', 'Se guardo Exitosamente', function(){
                                location.reload();
                            });
                        }
                        if (res== 0) {
                            alertify.alert('Alerta', 'Error al guardar');
                        }
                    }
                });
            } 
        });

        $("#btnCreditos").click(function () {
            var monto = $("#monto").val();
            var plazo = $("#plazo").val();
            var cuota = $("#cuota").val();
            var tasa = $("#tasa").val();
            monto = monto.replace(/,/g, "");
            if (monto.length=="") {
                alertify.alert('Alerta', 'Escriba el monto que prestara.');
            }else if (plazo.length=="") {
                alertify.alert('Alerta', 'Escriba el plazo que prestara.');
            }else if (cuota==0) {
                alertify.alert('Alerta', 'Revise la cuota.');
            }else if (tasa.length=="") {
                alertify.alert('Alerta', 'Revise la tasa.');
            }
            else{
                $("#collapseExample").show();
                $("#btnRegistrarC").show();
            }
        });

    });

function validarEmail() {
    var email =  document.getElementById('email').value;
    var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!regex.test(email)) {
        alertify.alert('Alerta', 'Ingrese un correo electronico valido'); 
    }
}
</script>