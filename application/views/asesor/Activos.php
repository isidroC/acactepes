<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
<!--link rel="stylesheet" type="text/css" href="./bootstrap/css/jquery.dataTables.css"-->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap.css">


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.css">

<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">Tipos de Clientes</h1>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="container">
            <br>
            <div class="table-wrap" id="dvTable" >
                <div class="table-responsive">
                    <table id="activos" class="table table-striped table-bordered" >
                        <thead>
                            <tr>
                                <th class="text-center">Dui</th>
                                <th class="text-center">Nombre Completo</th>
                                <th class="text-center">Nit</th>
                                <th class="text-center">Telefono</th>
                                <th class="text-center">Observacion</th>
                                <th class="text-center">Fecha de Registro</th>
                                <th class="text-center">Estado de proceso</th>
                                <th class="text-center">Ver</th>
                            </tr>
                        </thead>
                        <tbody id="tbodyActivos">
                            <?php foreach ($clientes as $c) { ?>
                                <tr>
                                    <td class="text-center" id="dui"><?= $c->dui; ?></td>
                                    <td class="text-center"><?= $c->nombres." ".$c->apellidos; ?></td>
                                    <td class="text-center"><?= $c->nit; ?></td>
                                    <td class="text-center"><?= $c->telefono; ?></td>
                                    <td class="text-center">
                                        <?php if ($c->id_estado==3) {
                                            echo "Sin Observacion";
                                        }else{
                                            echo $c->opservacion;
                                        } ?>

                                    </td>
                                    <td class="text-center"><?= $c->fecha_creacion; ?></td>
                                    <td class="text-center"><?= $c->estado; ?></td>
                                    <td class="text-center"><a href="<?= base_url('AsesorC/getCliente/'.$c->id_cliente) ?>" onclick="return confirm('¿Estas seguro que desea entrar a este perfil de cliente?')"><i class="fas fa-eye"></i></td>
                                </tr>
                            <?php  } ?>

                        </tbody>
                    </table>
                    <br>
                    <a style="float: left;" href="<?= base_url('AsesorC')?>" onclick="return confirm('¿Seguro que quieres Registrar un credito nuevo y cliente?')" type="button" class="btn btn-default">Registrar Creditos</a>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
<!--script type="text/javascript" src="./bootstrap/js/jquery.dataTables.min.js"></script-->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>

<script type="text/javascript">
    $('#activos').DataTable({
        dom: 'Bfrtip',
        buttons: [
        'copy', 'csv', 'excel', 'print'
        ],

        "iDisplayLength": 5,

        "aLengthMenu": [[5, 5], [5, 5]],

        "oLanguage": 

        {

            "sLengthMenu": "_MENU_ Registros por p&aacute;gina"

        } ,

    });  

    /*mostrar();

    function mostrar(){
        var url = '<?= base_url('AsesorC/tipoClientes') ?>';
        return $.ajax({

            url: url,

            contentType: "application/json; charset=utf-8",

            dataType: "json",

            success: function (msg) {

                var d;

                document.getElementById('dvTable').style.display = 'block';


                $.each(msg,function(index,obj){
                    console.log(obj);
                    var id = obj.id_cliente;

                    var completo = obj.nombres+" "+obj.Apellidos;
                    var telefono = obj.telefono;
                    var dui  = obj.dui;
                    var nit = obj.nit;
                    var opservacion = obj.opservacion;
                    var fecha = obj.fecha_creacion;
                    var estado = obj.estado_analisis;

                    id = "'"+id+"'";

                    d+= '<tr>'+

                    '<td class="text-center">'+dui+'</td>'+

                    '<td class="text-center">'+completo+'</td>'+

                    '<td class="text-center">'+nit+'</td>'+

                    '<td class="text-center">'+telefono+'</td>'+

                    '<td class="text-center">'+opservacion+'</td>'+

                    '<td class="text-center">'+fecha+'</td>'+

                    '<td class="text-center">'+estado+'</td>'+

                    '<td class="text-center">'+id+'</td>'+

                    '</tr>';

                });

                $("#activos").DataTable().fnDestroy();

                $("#tbodyActivos").empty();

                $("#activos").append(d);

                $('#activos').DataTable(

                {
                    dom: 'Bfrtip',
                    buttons: [
                    'copy', 'csv', 'excel', 'print'
                    ],

                    "iDisplayLength": 5,

                    "aLengthMenu": [[5, 5], [5, 5]],

                    "oLanguage": 

                    {

                        "sLengthMenu": "_MENU_ Registros por p&aacute;gina"

                    } ,



                });



            }

        });
    }*/

</script>