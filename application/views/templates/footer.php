</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
<!-- Scripts -->
<script type="text/javascript">
	function numeros(e) {
		var key = window.Event ? e.which : e.keyCode;
		return ((key >= 48 && key <= 57) || (key==8));
	}
	function soloLetras(e){
		key = e.keyCode || e.which;
		tecla = String.fromCharCode(key).toLowerCase();
		letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
		especiales = "8-37-39-46";

		tecla_especial = false
		for(var i in especiales){
			if(key == especiales[i]){
				tecla_especial = true;
				break;
			}
		}

		if(letras.indexOf(tecla)==-1 && !tecla_especial){
			return false;
		}
	}
</script>
<script src="<?= base_url('props/js/app.js') ?>"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?= base_url('props/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js') ?>"></script> 
<!--Wave Effects -->
<script src="<?= base_url('props/js/waves.js') ?>"></script>
<!--Menu sidebar -->
<script src="<?= base_url('props/js/sidebarmenu.js') ?>"></script>
<!--Custom JavaScript -->
<script src="<?= base_url('props/js/custom.min.js') ?>"></script>
<!--This page JavaScript -->
<!-- <script src="dist/js/pages/dashboards/dashboard1.js"></script> -->

</body>
</html>