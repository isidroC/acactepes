
<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header" data-logobg="skin5">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <a class="navbar-brand" href="index.html">
                <!-- Logo icon -->
                <b class="logo-icon p-l-10">
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="<?= base_url('props/image/logo.png') ?>" alt="homepage" class="light-logo" />

                </b>
                <!--End Logo icon -->
                <!-- Logo text -->
                <span class="logo-text">
                 <!-- dark Logo text -->
                 <b>Acactepes</b>
             </span>
             <!-- Logo icon -->
             <!-- <b class="logo-icon"> -->
                <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                <!-- Dark Logo icon -->
                <!-- <img src="assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->

                <!-- </b> -->
                <!--End Logo icon -->
            </a>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Toggle which is visible on mobile only -->
            <!-- ============================================================== -->
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-left mr-auto">
                <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>

            </ul>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-right">
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown" >
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span><b>Bienvenido: <?= $this->session->userdata('nombre'); ?></b></span>
                    </a>
                </li>
                <li class="nav-item dropdown" title="Perfil">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?= base_url('props/images/users/1.jpg') ?>" alt="user" class="rounded-circle" width="31">
                    </a>
                </li>
                <li class="nav-item d-none d-md-block" title="Cerrar Sesion" style="background-color: #C61A3A;"><a class="nav-link sidebartoggler waves-effect waves-light"  href="<?= base_url('login/logout')?>" onclick="return confirm('¿Esta Seguro que deseas cerrar Sesion?')" data-sidebartype="mini-sidebar">Cerrar Sesion  <i class="fas fa-arrow-right"></a></i>

                    <!-- ============================================================== -->
                    <!-- User profile and search -->
                    <!-- ============================================================== -->
                </ul>
            </div>
        </nav>
    </header>
    <!-- ============================================================== -->
    <!-- End Topbar header 096baa-->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!--38914f Left Sidebar - style you can find in sidebar.scss 1F262D -->
    <!-- ============================================================== -->
    <aside class="left-sidebar" data-sidebarbg="skin5">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- Sidebar navigation-->
            <nav class="sidebar-nav">
                <ul id="sidebarnav" class="p-t-30">
                    <?php if ($this->session->userdata('rol')==2 || $this->session->userdata('rol')==1): ?>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-dollar-sign"></i><span class="hide-menu">Gerencia </span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="<?= base_url('gerenciaC')?>" class="sidebar-link"><i class="fas fa-clipboard-list"></i><span class="hide-menu"> Cosas de gerente </span></a></li>
                        </ul>
                    </li>
                    <?php endif ?>
                    <?php if ($this->session->userdata('rol')==5|| $this->session->userdata('rol')==1): ?>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-dollar-sign"></i><span class="hide-menu">Asesor </span></a>
                        <ul aria-expanded="false" class="collapse  first-level">

                            <li class="sidebar-item"><a href="<?= base_url('asesorC')?>" class="sidebar-link"><i class="fas fa-clipboard-list"></i><span class="hide-menu"> Asesor de Ventas </span></a></li>
                            <li class="sidebar-item"><a href="<?= base_url('asesorC/activos')?>" class="sidebar-link"><i class="fas fa-clipboard-list"></i><span class="hide-menu"> Registos activos </span></a></li>
                        </ul>
                        
                    </li>
                    <?php endif ?>
                    <?php if ($this->session->userdata('rol')==6 || $this->session->userdata('rol')==1): ?>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-cash-register"></i><span class="hide-menu">Cajeros </span></a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item"><a href="<?= base_url('cajeroC')?>" class="sidebar-link"><i class="fas fa-file-invoice"></i><span class="hide-menu"> Facturas </span></a></li>
                        </ul>
                    </li>
                <?php endif ?>
                <?php if ($this->session->userdata('rol')==4 || $this->session->userdata('rol')==1): ?>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-warehouse"></i><span class="hide-menu">Analista </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="<?= base_url('analistaC')?>" class="sidebar-link"><i class="fas fa-clipboard-list"></i><span class="hide-menu"> Analista de Creditos </span></a></li>
                    </ul>
                </li>
            <?php endif ?>

                <!-- <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-mail-bulk"></i><span class="hide-menu">Cobros </span></a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item"><a href="<?= base_url('cobrosC')?>" class="sidebar-link"><i class="fas fa-share"></i><span class="hide-menu"> Cobro de pagos </span></a></li>
                    </ul>
                </li> -->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->