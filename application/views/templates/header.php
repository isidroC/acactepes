<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('props/image/nose.png')?>">
    <title><?= $title; ?></title>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('props/bootstrap/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?php echo base_url('props/alertifyjs/css/alertify.min.css');?>">
    <link rel="stylesheet" href="<?php echo base_url('props/alertifyjs/css/themes/default.min.css');?>">
    <link  href="<?= base_url('props/css/style.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('props/css/colors.css') ?>" rel="stylesheet"> 
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!--DATA TABLE  -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css"/>


    <!-- Font Awesome -->  
    <script src="https://kit.fontawesome.com/bfd68742ca.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="<?= base_url('props/bootstrap/js/jquery.js')?>"></script>
    <script type="text/javascript" src="<?= base_url('props/bootstrap/js/bootstrap.min.js')?>"></script>
    <script src="<?php echo base_url('props/alertifyjs/alertify.min.js');?>"></script>
    <!-- esta es el de input mask -->
    <script src="<?= base_url('props/js/jquery.maskedinput.js') ?>"></script>
    <script src="<?= base_url('props/js/jquery.maskMoney.js') ?>"></script>

    <!-- DATA TABLE -->
    <script type="text/javascript" src="<?= base_url('props/dataTable/datatables.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('props/dataTable/DataTables/js/jquery.dataTables.min.js') ?>"></script>          
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- aqui pondremos el menu  -->
        <!-- ============================================================== -->
        <?php $this->load->view('templates/navbar') ?>

