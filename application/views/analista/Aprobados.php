<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="http://www.datatables.net/rss.xml">
<!--link rel="stylesheet" type="text/css" href="./bootstrap/css/jquery.dataTables.css"-->

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.bootstrap.css">


<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.css">

<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-12 d-flex no-block align-items-center">
                <h1 class="page-title">Detalles de Clientes</h1>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="container">
            <div>
                <br><br>
                <button type="button" data-toggle="modal" data-target="#modalOpciones" class="btn btn-primary btn-lg btn-block">Finalizar Proceso</button>
                <br><br>
            </div>
            <br> 
            <div class="table-wrap" id="dvTable" >
                <div class="table-responsive">

                    <table class="table table-striped table-bordered" >
                        <thead>
                            <tr><td colspan="7"><center><h2>Detalles de cliente</h2></center></td></tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Dui</td>
                                <td class="text-center">Nombre Completo</td>
                                <td class="text-center">Nit</td>
                                <td class="text-center">Telefono</td>
                                <td class="text-center">Fecha de Nacimiento</td>
                                <td class="text-center">Sexo</td>
                                <td class="text-center">Departamento</td>
                                <td class="text-center">Municipio</td>
                                <td class="text-center">Direccion</td>
                                <td class="text-center">Profecion</td>
                                <td class="text-center">Ingresos</td>
                                <td class="text-center">Email</td>
                            </tr>
                        </tbody>
                        <tbody id="tbodyActivos">
                            <?php foreach ($cliente as $c) { ?>
                                <tr>
                                    <td class="text-center"><?= $c->dui; ?></td>
                                    <td class="text-center"><?= $c->cliente; ?></td>
                                    <td class="text-center"><?= $c->nit; ?></td>
                                    <td class="text-center"><?= $c->telefono; ?></td>
                                    <td class="text-center"><?= $c->fecha_nacimiento ?></td>
                                    <td class="text-center"><?= $c->sexo ?></td>
                                    <td class="text-center"><?= $c->departamento ?></td>
                                    <td class="text-center"><?= $c->municipio ?></td>
                                    <td class="text-center"><?= $c->direccion ?></td>
                                    <td class="text-center"><?= $c->profecion ?></td>
                                    <td class="text-center"><?= $c->ingresos ?></td>
                                    <td class="text-center"><?= $c->email ?></td>
                                </tr>
                            <?php  } ?>

                        </tbody>
                        <thead>
                            <tr><td colspan="5"><center><h2>Detalles de Credito</h2></center></td></tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Monto</td>
                                <td class="text-center">Plazo</td>
                                <td class="text-center">Cuota</td>
                                <td class="text-center">Tasa</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <?php foreach ($Credito as $c) { ?>
                                <tr>
                                    <td class="text-center"><?= $c->monto; ?></td>
                                    <td class="text-center"><?= $c->plazo; ?></td>
                                    <td class="text-center"><?= $c->cuota; ?></td>
                                    <td class="text-center"><?= $c->tasa; ?></td>
                                </tr>
                            <?php  } ?>
                        </tbody>
                        <thead>
                            <tr><td colspan="5"><center><h2>Referidos</h2></center></td></tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">Nombre Completo</td>
                                <td class="text-center">Telefono</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <?php foreach ($Referido as $c) { ?>
                                <tr>
                                    <td class="text-center"><?= $c->nombre; ?></td>
                                    <td class="text-center"><?= $c->telefono; ?></td>
                                </tr>
                            <?php  } ?>
                        </tbody>
                        <thead>
                            <tr><td colspan="5"><center><h2>Tramites en proceso</h2></center></td></tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th class="text-center">Observacion</th>
                                <th class="text-center">Fecha de Registro</th>
                                <th class="text-center">Estado de proceso</th>
                            </tr>
                        </tbody>
                        <tbody id="tbodyActivos">
                            <?php foreach ($Analisis as $c) { ?>
                                <tr>
                                    <td class="text-center">
                                        <?php if ($c->estado_analisis==3) {
                                            echo "Sin Observacion";
                                        }else{
                                            echo $c->opservacion;
                                        } ?>

                                    </td>
                                    <td class="text-center"><?= $c->fecha_creacion; ?></td>
                                    <td class="text-center">
                                        <?php if ($c->estado_analisis==3) {
                                            echo "En proceso de inspeccion";
                                        }elseif ($c->estado_analisis==2) {
                                            echo "denegado el prestamo";
                                        }else{
                                            echo "Aprobado listo para contrato";
                                        } ?>
                                        
                                    </td>
                                </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                    <br>

                </div>
            </div>
            <div>
                <br><br>
                <button type="button" data-toggle="modal" data-target="#modalOpciones" class="btn btn-primary btn-lg btn-block">Finalizar Proceso</button>
                <br><br>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    <footer class="footer text-center">
        All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
    </footer>
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
<!-- ============================================================== -->
<!--script type="text/javascript" src="./bootstrap/js/jquery.dataTables.min.js"></script-->
<!-- Modal -->
<div class="modal fade" id="modalOpciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h2 class="modal-title" id="exampleModalLabel">Observaciones</h2>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
      </button>
  </div>
  <div class="modal-body">
    <form method="POST" id="frmOP">
        <div class="form-group">
            
            <label>Agrege una Observacion*</label>
            <textarea name="observa" id="observa" style="width:100%;" class="form-control" maxlength="200" rows="3" tabindex="8" 
            style="width: 400px"></textarea>
            <input type="hidden" name="id" id="id" id="<?= $id ?>">
            <input type="hidden" name="id_analisis" id="id_analisis" value="<?= $datos->id_analisis?>">
        </div>
    </form>
</div>

<div class="modal-footer">
    <button type="button" id="btnAprobar" class="btn btn-primary" data-dismiss="modal">Aprobar Credito</button>
    <button type="button" id="btnDenegar" class="btn btn-danger">Denegar Credito</button>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.print.min.js"></script>
<script type="text/javascript">
    $("#btnAprobar").click(function () {
        if ($("#observa").val().length<=10) {
            alert('Ingrege una Observacion mayor a 10 caracteres');
        }else{
            $url = '<?= base_url("AnalistaC/Aprobar")?>';
            $data = $("#frmOP").serialize();
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: $url,
                data: $data,
                dataType: 'json',
                success: function (res) {
                    if (res==1) {
                        alertify.alert('Alerta', 'Credito Aprobado Exitosamente',function () {
                            window.location.href = "http://localhost/acactepes/AnalistaC/";
                        });
                    }else{
                        alertify.alert('Alerta', 'Error en Proceso');
                        
                    }
                }
            });
        }
    });


    $("#btnDenegar").click(function () {
        if ($("#observa").val().length<=10) {
            alert('Ingrege una Observacion mayor a 10 caracteres');
        }else{
            $("#frmOP").attr('action','<?= base_url("AnalistaC/Negar")?>');
            $url = $("#frmOP").attr('action');
            $data = $("#frmOP").serialize();
            $.ajax({
                type: 'ajax',
                method: 'post',
                url: $url,
                data: $data,
                dataType: 'json',
                success: function (res) {
                    if (res==1) {
                        alertify.alert('Alerta', 'Credito Negado Exitosamente',function () {
                            window.location.href = "http://localhost/acactepes/AnalistaC/";
                        });
                    }else{
                        alertify.alert('Alerta', 'Error en Proceso');
                        
                    }
                }
            });
        }
    });
</script>